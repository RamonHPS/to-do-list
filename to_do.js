var myList = [];

function lines(line,id, inputValue) {
    this.id = id;
    this.complete = false;
    this.line = line;
    this.value = inputValue;
}

function addTask(elementValue) {
    var line = document.createElement("li");
    var inputValue = elementValue || document.getElementById("myTask").value;
    var textNode = document.createTextNode(inputValue); 
    line.appendChild(textNode);
   
    document.getElementById("myList").appendChild(line);
    if (elementValue === undefined) {
        document.getElementById("myTask").value = "";
        
    }else{
        document.getElementById("myEdit").value = "";  
    }
    
    var myLine = new lines(line,myList.length,inputValue);
    myList.push(myLine);
    addRemoveOption(myLine);
    addEditTaskOption(myLine,inputValue);
    addCompleteTaskOption(myLine);
    saveList(myList);  
    completedTask(elementValue,myLine);
    elementFocus("myTask");
}

function editTask(){
    var element = document.getElementById("myEdit")
    var editedValue = element.value;
    if (editedValue != ""){
        var id = element.getId;
        var lineElement = document.getElementsByTagName("li");

        lineElement[id].innerHTML = editedValue;

        element.value = "";
        var myLine = new lines(lineElement[id],id,editedValue);
        addRemoveOption(myLine);
        addEditTaskOption(myLine,editedValue);

        myList.splice(id,1,myLine);
        saveList(myList);  
        elementFocus("myTask");
        elementDisable("myEdit",true);
    }
}

function addEditTaskOption(myLine,inputValue) {
    var edit = document.createElement("SPAN");
    var editIcon = document.createTextNode("Edit");
    edit.className = "edit";
    edit.onclick = function () {
        var edit = document.getElementById("myEdit");
        edit.value = inputValue;
        edit.getId = myLine.id;
        elementDisable("myEdit",false);
        elementFocus("myEdit")
    };
    edit.appendChild(editIcon);
    myLine.line.appendChild(edit);   
}

function addCompleteTaskOption(myLine) {
     myLine.line.addEventListener('click', function (ev){
         if(ev.target.tagName === 'LI'){
             ev.target.classList.toggle('checked');
         }
         myLine.complete = true;
     }, false)
}

function addRemoveOption(myLine) {
    var remove = document.createElement("SPAN");
    var removeIcon = document.createTextNode("\u00D7");
    remove.className = "remove";
    remove.onclick = function () {
        this.parentElement.parentElement.removeChild(this.parentElement);
        removeTask(myLine.id);
    };
    remove.appendChild(removeIcon);
    myLine.line.appendChild(remove);
}


function removeTask(id) {//arrumar
    console.log('id '+id);
    var element = myList.findIndex(i => i.id === id);
    myList.splice(element,1);
    saveList(myList);
}

function removeAllElements() {
    var elements = document.getElementsByClassName("remove");
    for(var i = elements.length-1; i >= 0; i--){
        elements[i].click();
    }
}

function saveList(obj) {
    localStorage.setItem("taskList", JSON.stringify(obj));
}

function getList() {
    return JSON.parse(localStorage.getItem("taskList"));
}

function showList(){
    console.log(JSON.parse(localStorage.getItem("taskList")));
}

function retrieveAllList() {
    var myLastList = getList();
    for(var i = 0; i < myLastList.length; i++){
       addTask(myLastList[i].value);
    }
}

function elementFocus(element){
    document.getElementById(element).focus();
}

function elementDisable(element,boolean){
    var option = false || boolean;
    document.getElementById(element).disabled = option;
}

function completedTask(elementValue,myLine){
    if (elementValue){
        if(myLine.complete === true){
            this.clicl();
        }
    }
}
